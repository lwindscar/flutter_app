import 'package:flutter/material.dart';


class PostItem extends StatelessWidget {

  final String title;
  final String content;
  final Function onPress;

  PostItem({
    @required this.title,
    @required this.content,
    @required this.onPress,
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            title: Text(title),
          ),
          Text(content),
          FlatButton(
            child: Text('Go'),
            onPressed: onPress,
          )
        ],
      ),
    );
  }
}