import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import './bloc.dart';

import 'package:flutter_app/repositories/repositories.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final UserRepository _userRepository;

  AuthBloc({@required UserRepository userRepository})
    : assert(userRepository != null),
      _userRepository = userRepository;

  @override
  AuthState get initialState => Uninitialized();

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    // TODO: Add Logic
    if (event is AppStarted) {
      yield* _mapAppStartedToState();
    } else if (event is LoggedIn) {
      yield* _mapLoggedInToState();
    } else if (event is LoggedOut) {
      yield* _mapLoggedOutToState();
    }
  }

  Stream<AuthState> _mapAppStartedToState() async* {
    try {
      final currentUser = await _userRepository.getCurrentUser();
      print(currentUser);
      if (currentUser != null) {
        yield Authenticated(currentUser.name);
      } else {
        yield Unauthenticated();
      }
    } catch(_) {
      yield Unauthenticated();
    }
  }

  Stream<AuthState> _mapLoggedInToState() async* {
    yield Authenticated('RRR');
  }

  Stream<AuthState> _mapLoggedOutToState() async* {
    yield Unauthenticated();
    await _userRepository.signOut();
  }
}
