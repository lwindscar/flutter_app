//import 'package:meta/meta.dart';
import 'package:dio/dio.dart';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_app/utils/app_config.dart';

class ApiClient {
  Dio _dio = new Dio(new BaseOptions(
    baseUrl: AppConfig.apiEndpoint,
    connectTimeout: 5000,
    receiveTimeout: 10000,
    contentType: ContentType.json,
    responseType: ResponseType.json,
  ));

  request(String url, { String method = 'GET', Map data }) async {
    Map _queryParams;
    Map _formData;
    if (method == 'GET') {
      _queryParams = data;
    } else if (method == 'POST' || method == 'PUT') {
      _formData = data;
    }

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String _token = prefs.getString('token') ?? '';
    String _authHeader = 'Bearer $_token';

    final Response response = await _dio.request(url,
      queryParameters: _queryParams,
      data: _formData,
      options: Options(
        method: method,
        headers: {
          HttpHeaders.authorizationHeader: _authHeader
        }
      ),
    );
    print('''***responseData***
      ${response.data}''');
    return response.data;
  }
}

