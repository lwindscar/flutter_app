//import 'package:meta/meta.dart';

import 'package:flutter_app/models/models.dart';
import 'package:flutter_app/utils/api.dart';
//import 'package:flutter_app/repositories/repositories.dart';

class PostRepository {
  final ApiClient _apiClient;

  PostRepository({ApiClient apiClient})
      : _apiClient = apiClient ?? ApiClient();

  Future<Post> create(Map data) async {
    Map resData = await _apiClient.request('/posts',
      method: 'POST',
      data: data,
    );
    return Post(
      id: resData['id'],
      title: resData['title'],
      content: resData['content'],
    );
  }

}
