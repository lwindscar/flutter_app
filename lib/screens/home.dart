import 'package:flutter/material.dart';
import './screens.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _activeTabIndex = 1;

  void _onPressTap(int index) {
    setState(() {
      _activeTabIndex = index;
    });
  }

  void _onPressAddBtn() {
    Navigator.push(context, new MaterialPageRoute(builder: (context) {
      return CreatePostPage();
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        shape: CircularNotchedRectangle(),
        child: Row(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () => _onPressTap(1),
            ),
            SizedBox(),
            IconButton(
              icon: Icon(Icons.person),
              onPressed: () => _onPressTap(2),
            )
          ],
          mainAxisAlignment: MainAxisAlignment.spaceAround,
        ),
      ),
      body: _activeTabIndex == 2 ? MinePage() : PostPage(),
      floatingActionButton: FloatingActionButton(
        onPressed: _onPressAddBtn,
        tooltip: 'Add Post',
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}