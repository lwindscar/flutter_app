import 'package:meta/meta.dart';
import 'package:flutter_app/models/models.dart';

@immutable
class LoginState {
  final User user;
  final bool loading;
  final String error;

  LoginState({
    @required this.user,
    this.loading = false,
    this.error,
  });

  factory LoginState.initial() {
    return LoginState(user: null);
  }

  factory LoginState.loading() {
    return LoginState(user: null, loading: true);
  }

  factory LoginState.update(User user) {
    return LoginState(user: user);
  }

  factory LoginState.success(User user) {
    return LoginState(user: user, loading: false);
  }

  factory LoginState.failure(String error) {
    return LoginState(user: null, loading: false, error: error);
  }

  @override
  String toString() {
    final String nickname = user?.name;
    return '''LoginState {
      user: $nickname,
      loading: $loading,
    }''';
  }
}
