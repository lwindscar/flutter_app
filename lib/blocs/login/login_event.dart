import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';

@immutable
abstract class LoginEvent extends Equatable {
  LoginEvent([List props = const []]) : super(props);
}

class Submitted extends LoginEvent {
  final String username;
  final String password;
  final BuildContext context;

  Submitted({
    @required this.username,
    @required this.password,
    @required this.context,
  }): super([username, password, context]);

  @override
  String toString() {
    return 'Submitted { email: $username, password: $password }';
  }
}
