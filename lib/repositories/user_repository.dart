//import 'package:meta/meta.dart';

import 'package:flutter_app/models/models.dart';
import 'package:flutter_app/utils/api.dart';
//import 'package:flutter_app/repositories/repositories.dart';

class UserRepository {
  final ApiClient _apiClient;

  UserRepository({ApiClient apiClient})
    : _apiClient = apiClient ?? ApiClient();

  Future<LoginResponse> signIn({String username, String password}) async {
    Map resData = await _apiClient.request('/auth/login',
      method: 'POST',
      data: {
        'username': username,
        'password': password,
      }
    );
    User user = User(
      id: resData['user']['id'],
      username: resData['user']['username'],
      name: resData['user']['name'],
    );
    return LoginResponse(token: resData['token'], user: user);
  }

  Future<User> signUp(Map data) async {
    return await _apiClient.request('/users',
      method: 'POST',
      data: data,
    );
  }

  Future signOut() async {}

  Future<User> getCurrentUser() async {
    Map resData = await _apiClient.request('/users/current');
    if (resData == null) {
      return null;
    }
    return User(
      id: resData['id'],
      username: resData['username'],
      name: resData['name'],
      email: resData['email'],
      phone: resData['phone'],
    );
  }
}
