import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext content) {
    return Scaffold(
      body: Center(
        child: Text(
          'Splash Loading...',
          style: TextStyle(fontSize: 28),
        ),
      ),
    );
  }
}
