import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';

class RegisterPage extends StatefulWidget {
  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<RegisterPage> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  _onSubmit() async {
    if (_formKey.currentState.validate()) {
      print('submitted');
    }
  }

  _onReset(){
    _formKey.currentState.reset();
  }

  @override
  Widget build(BuildContext content) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: FormBuilder(
        key: _formKey,
        autovalidate: true,
        // readonly: true,
        child: ListView(
          children: <Widget>[
            FormBuilderTextField(
              attribute: 'username',
              decoration: InputDecoration(
                labelText: 'Username',
                icon: Icon(Icons.person),
              ),
              validators: [
                FormBuilderValidators.required(),
                FormBuilderValidators.minLength(6),
              ],
            ),
            FormBuilderTextField(
              attribute: 'password',
              decoration: InputDecoration(
                labelText: 'Password',
                icon: Icon(Icons.lock),
              ),
              obscureText: true,
              validators: [
                FormBuilderValidators.required(),
                FormBuilderValidators.minLength(8),
              ],
            ),
            FormBuilderTextField(
              attribute: 'repassword',
              decoration: InputDecoration(
                labelText: 'RePassword',
                icon: Icon(Icons.lock),
              ),
              obscureText: true,
              validators: [
                FormBuilderValidators.required(),
                FormBuilderValidators.minLength(8),
              ],
            ),
            FormBuilderTextField(
              attribute: 'name',
              decoration: InputDecoration(
                labelText: 'Name',
                icon: Icon(Icons.perm_identity),
              ),
              validators: [
                FormBuilderValidators.required(),
              ],
            ),
            FormBuilderDateTimePicker(
              attribute: 'birthday',
              inputType: InputType.date,
              format: DateFormat('yyyy-MM-dd'),
              decoration: InputDecoration(
                labelText: 'Birthday',
                icon: Icon(Icons.cake),
              ),
            ),
            FormBuilderTextField(
              attribute: 'email',
              decoration: InputDecoration(
                labelText: 'Email',
                icon: Icon(Icons.email),
              ),
              validators: [
                FormBuilderValidators.email(),
              ],
            ),
            FormBuilderTextField(
              attribute: 'phone',
              decoration: InputDecoration(
                labelText: 'Phone',
                icon: Icon(Icons.phone),
              ),
              validators: [
                FormBuilderValidators.pattern(
                  RegExp(r'^1(3|4|5|7|8)\d{9}$')
                ),
              ],
            ),
            FormBuilderRadio(
              decoration: InputDecoration(
                labelText: 'Gender',
                icon: Icon(Icons.transit_enterexit),
              ),
              leadingInput: true,
              attribute: 'gender',
              options: ['male', 'female']
                .map((lang) => FormBuilderFieldOption(value: lang))
                .toList(growable: false),
            ),
            FormBuilderCheckboxList(
              decoration: InputDecoration(
                labelText: 'Languages',
                icon: Icon(Icons.language),
              ),
              attribute: 'languages',
              initialValue: ['Chinese'],
              options: ['Chinese', 'English', 'French', 'Spanish', 'Japanese']
                .map((lang) => FormBuilderFieldOption(value: lang))
                .toList(growable: false),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: MaterialButton(
                    color: Theme.of(context).accentColor,
                    child: Text(
                      'Submit',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: _onSubmit,
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: MaterialButton(
                    color: Colors.redAccent,
                    child: Text(
                      'Reset',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: _onReset,
                  ),
                ),
              ],
            )
          ],
        ),
      )
    );
  }
}
