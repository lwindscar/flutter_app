import 'package:flutter/material.dart';
import 'package:flutter_app/repositories/repositories.dart';
import 'package:flutter_app/models/models.dart';

class CreatePostPage extends StatefulWidget {
  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<CreatePostPage> {
  Map _formData = {};
  PostRepository _postRepository = PostRepository();


  _onSubmit() async {
    try {
      Post post = await _postRepository.create(_formData);
      print(post);
      Navigator.pop(context);
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext content) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create Post'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              autofocus: false,
              decoration: InputDecoration(
                  labelText: '标题', prefixIcon: Icon(Icons.title)),
              onChanged: (v) {
                setState(() {
                  _formData['title'] = v;
                });
              },
            ),
            TextField(
              decoration: InputDecoration(
                  labelText: '内容', prefixIcon: Icon(Icons.text_fields)),
              onChanged: (v) {
                setState(() {
                  _formData['content'] = v;
                });
              },
            ),
            Padding(
              padding: const EdgeInsets.only(top: 28.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      padding: EdgeInsets.all(15.0),
                      child: Text('提交'),
                      color: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      onPressed: _onSubmit,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
