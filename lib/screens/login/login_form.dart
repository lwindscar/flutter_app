import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_app/blocs/blocs.dart';
import 'package:flutter_app/screens/screens.dart';
import './login_button.dart';

class LoginForm extends StatefulWidget {
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  LoginBloc _loginBloc;

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _loginBloc,
      listener: (BuildContext context, LoginState state) {
        if (state.loading) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Logging In...'),
                    CircularProgressIndicator(),
                  ],
                )
              )
            );
        }
        if (state.error != null) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[Text(state.error), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: Padding(
        padding: EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: Image.asset('assets/flutter_logo.png', height: 200),
              ),
              TextFormField(
                controller: _usernameController,
                decoration: InputDecoration(
                  icon: Icon(Icons.person),
                  labelText: 'Username',
                ),
                validator: (v) {
                  return v.isNotEmpty ? null : 'username is required';
                },
              ),
              TextFormField(
                controller: _passwordController,
                decoration: InputDecoration(
                  icon: Icon(Icons.lock),
                  labelText: 'Password',
                ),
                obscureText: true,
                validator: (v) {
                  if (v.isEmpty) {
                    return 'password is required';
                  }
                  if (v.length < 8) {
                    return 'password should be at least 8 characters';
                  }
                  return null;
                },
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    LoginButton(onPressed: () => _onFormSubmitted(context)),
                    FlatButton(
                      child: Text(
                        'Create an account',
                        style: TextStyle(color: Colors.lightBlue),
                      ),
                      onPressed: _navToRegisterPage,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onFormSubmitted(BuildContext context) async {
    if (_formKey.currentState.validate()) {
      _loginBloc.dispatch(
        Submitted(
          username: _usernameController.text,
          password: _passwordController.text,
          context: context,
        )
      );
    }
  }

  void _navToRegisterPage() {
    print('navToRegisterPage');
    Navigator.push(context,
      MaterialPageRoute(builder: (BuildContext context) {
        return RegisterPage();
      })
    );
  }
}