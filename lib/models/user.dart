import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User extends Equatable {
  final String id;
  final String username;
  final String name;
  final String email;
  final String phone;

  User({this.id, this.username, this.name, this.email, this.phone})
      : super([id, username, name, email, phone]);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  String toString() => 'User { id: $id }';
}

class LoginResponse extends Equatable {
  final String token;
  final User user;

  LoginResponse({this.token, this.user}): super([token, user]);

  @override
  String toString() => 'Token $token';
}
