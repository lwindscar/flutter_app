import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/repositories/repositories.dart';
import 'package:flutter_app/models/models.dart';
import 'package:flutter_app/screens/screens.dart';
import './bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository _userRepository = UserRepository();

  @override
  LoginState get initialState => LoginState.initial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    // TODO: Add Logic
    if (event is Submitted) {
      yield* _login(event);
    }
  }

  Stream<LoginState> _login(Submitted event) async* {
    yield LoginState.loading();
    try {
      LoginResponse res = await _userRepository.signIn(
        username: event.username,
        password: event.password,
      );
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('token', res.token);
      await Future.delayed(Duration(seconds: 2));
      yield LoginState.success(res.user);
      Navigator.of(event.context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) => HomePage(title: res.user.name)
        )
      );
    } catch(_) {
      yield LoginState.failure('Login Failure');
    }
  }
}
