import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import './screens/screens.dart';
import './blocs/blocs.dart';
import './repositories/repositories.dart';

void main() {
  BlocSupervisor().delegate = AppBlocDelegate();
  runApp(App());
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final UserRepository _userRepository = UserRepository();
  AuthBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = AuthBloc(userRepository: _userRepository);
    _authBloc.dispatch(AppStarted());
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: _authBloc,
      child: MaterialApp(
        title: 'Flutter App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: BlocBuilder(
          bloc: _authBloc,
          builder: (BuildContext context, AuthState state) {
            if (state is Uninitialized) {
              return SplashPage();
            }
            if (state is Unauthenticated) {
              return LoginPage(userRepository: _userRepository);
            }
            return HomePage(title: (state as Authenticated).displayName);
          }),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _authBloc.dispose();
    super.dispose();
  }
}
